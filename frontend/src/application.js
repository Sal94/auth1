import React, { Component } from 'react';
import Shoppinglist from './shoppinglist';
import Loading from './loading';
import Login from './login';
import client from './feathers';

class Application extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  
  componentDidMount() {
    // Try to authenticate with the JWT stored in localStorage
    client.authenticate().catch(() => this.setState({ login: null }));
    client.on('authenticated', login => {
      
      // ... update the state
      this.setState({ login });
    });
  }
  
  render() {
    if ( this.state.login === undefined) {
      return(<Loading />);
    } else if ( this.state.login ) {
      return(<Shoppinglist />);
    }
    return(<Login />);
  }
}

export default Application;